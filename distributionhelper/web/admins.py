from pyramid.security import Deny, Allow, Everyone

from websauna.system.admin.modeladmin import ModelAdmin, model_admin
from websauna.system.crud.urlmapper import IdMapper

from distributionhelper.models import (
    Account, Recipient, Message, Worker, MessageDelivery, Campaign, CampaignWorker)


@model_admin(traverse_id="workers")
class WorkerAdmin(ModelAdmin):
    title = "Worker"
    model = Worker
    singular_name = "worker"
    plural_name = "workers"

    mapper = IdMapper(mapping_attribute="id")

    class Resource(ModelAdmin.Resource):
        def get_title(self):
            return self.get_object()

#
# @model_admin(traverse_id="campaign-workers")
# class CampaignWorkerAdmin(ModelAdmin):
#     title = "Workers"
#     model = CampaignWorker
#     singular_name = "worker"
#     plural_name = "workers"
#
#     mapper = IdMapper(mapping_attribute="id")
#
#     class Resource(ModelAdmin.Resource):
#         def get_title(self):
#             return self.get_object().name
#

@model_admin(traverse_id="campaigns")
class CampaignAdmin(ModelAdmin):
    title = "Campaign"
    model = Campaign
    singular_name = "campaign"
    plural_name = "campaigns"

    mapper = IdMapper(mapping_attribute="id")

    class Resource(ModelAdmin.Resource):
        def get_title(self):
            return self.get_object().name


@model_admin(traverse_id="message-delivery")
class MessageDeliveryAdmin(ModelAdmin):
    title = "Message Delivery"
    model = MessageDelivery
    singular_name = "message delivery"
    plural_name = "message deliveries"

    mapper = IdMapper(mapping_attribute="id")

    class Resource(ModelAdmin.Resource):
        def get_title(self):
            return self.get_object().status


@model_admin(traverse_id="accounts")
class AccountAdmin(ModelAdmin):
    title = "Account"
    model = Account
    singular_name = "account"
    plural_name = "accounts"

    mapper = IdMapper(mapping_attribute="id")

    class Resource(ModelAdmin.Resource):
        def get_title(self):
            return self.get_object().username


@model_admin(traverse_id="recipients")
class RecipientAdmin(ModelAdmin):
    title = "Recipient"
    model = Recipient
    singular_name = "recipient"
    plural_name = "recipients"

    mapper = IdMapper(mapping_attribute="id")

    class Resource(ModelAdmin.Resource):
        def get_title(self):
            return self.get_object().profile_url


@model_admin(traverse_id="messages")
class MessageAdmin(ModelAdmin):
    title = "Message"
    model = Message
    singular_name = "message"
    plural_name = "messages"

    mapper = IdMapper(mapping_attribute="id")

    class Resource(ModelAdmin.Resource):
        def get_title(self):
            return self.get_object().campaign
