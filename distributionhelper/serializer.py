import colander as co
from colander import Schema


class  SocialNetworksSchema(Schema):
    name = co.SchemaNode(co.String())
    login_url = co.SchemaNode(co.String())

class AccountSchema(Schema):
    username = co.SchemaNode(co.String())
    password = co.SchemaNode(co.String())
    profile_url = co.SchemaNode(co.String())
    social_networks = SocialNetworksSchema()
