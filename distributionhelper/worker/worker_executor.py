import typing as tp
import asyncio
import time
import concurrent.futures

from distributionhelper.social_manager.social import SocialYouTube
from distributionhelper.domain.entity import Action
from distributionhelper.worker.worker import BrowserWorker
from distributionhelper.worker.utils import get_workers, save_workers_states, restore_workers_states


def run_workers(workers: tp.List[BrowserWorker], actions_chain: tp.List[Action]):
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(workers)) as executor:
        futures = []
        for action in actions_chain:
            for w in workers:
                action_func = getattr(w.social, action.name)
                futures.append(executor.submit(action_func, *action.args))

        for idx, future in enumerate(concurrent.futures.as_completed(futures)):
            res = future.result()  # This will also raise any exceptions
            print("Processed job", idx, "result", res)
        futures = [executor.submit(worker.shutdown_webdriver) for worker in workers]


if __name__ == '__main__':
    socials = [
        SocialYouTube(username='skhalimon@gmail.com', password='******'),
        SocialYouTube(username='bitcoin19@gmail.com', password='******'),
    ]
    workers = get_workers(socials)
    workers_num = len(workers)
    actions_chain = [
        Action('login', ()),
        Action('watch_video', ('https://www.youtube.com/watch?v=_BTDWWEsDp4',))
    ]

    # executor = concurrent.futures.ThreadPoolExecutor(
    #     max_workers=workers_num,
    # )


    for w in workers:
        w.browser.visit('http://youtube.com')
    time.sleep(5)
    restore_workers_states(workers)
    run_workers(workers, actions_chain)

