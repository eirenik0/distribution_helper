from pyramid.security import Deny, Allow, Everyone

from websauna.system.admin.modeladmin import ModelAdmin, model_admin

from distributionhelper.models import Account

@model_admin(traverse_id="user-accounts")
class UserAccountAdmin(ModelAdmin):
    """Manage user owned accounts and their balances."""

    model = Account
