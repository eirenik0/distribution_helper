import re

import sqlalchemy as sa
from sqlalchemy import Table, Column, String, Integer, ForeignKey, Enum, Text
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import mapper, relationship
from sqlalchemy.ext.declarative import declared_attr

import deform
import colander

from websauna.system.model.meta import Base, metadata
from websauna.system.model.json import NestedMutationDict
from websauna.system.user.models import User

from websauna.system.model.columns import JSONB, UTCDateTime
from websauna.utils.time import now

from distributionhelper.domain.constants import (
    AvailableBrowsers, SocialNetworks, WorkerAvailabilityStatuses, MessageDeliveryStatuses, WorkerType,
    CampaignStatuses)


class BaseMixin(object):

    @declared_attr
    def __tablename__(cls):
        # convert BaseMixin to base_mixin
        splited_by_capital = re.sub(r"([A-Z])", r" \1",
                                    cls.__name__).split()
        return "_".join(splited_by_capital).lower()

    id = Column(Integer, autoincrement=True, primary_key=True)
    # uuid = Column(UUID(as_uuid=True),
    #               server_default=sa.text("uuid_generate_v4()"))


class CampaignWorker(Base):
    __table__ = Table('campaign_worker', Base.metadata,
                      Column('campaign_id', Integer,
                             ForeignKey('campaign.id'),
                             primary_key=True),
                      Column('worker_id', Integer,
                             ForeignKey('worker.id'),
                             primary_key=True)
                      )


class CampaignAccount(Base):
    __table__ = Table('campaign_account', Base.metadata,
                      Column('campaign_id', Integer,
                             ForeignKey('campaign.id'),
                             primary_key=True),
                      Column('account_id', Integer,
                             ForeignKey('account.id'),
                             primary_key=True)
                      )


class CampaignRecipient(Base):
    __table__ = Table('campaign_recipient', Base.metadata,
                      Column('campaign_id', Integer,
                             ForeignKey('campaign.id'),
                             primary_key=True),
                      Column('recipient_id', Integer,
                             ForeignKey('recipient.id'),
                             primary_key=True)
                      )


class Account(BaseMixin, Base):
    username = Column(String(50), nullable=False)
    hashed_password = Column('password', String(256), nullable=True)
    profile_url = Column(String(120), default=None)
    social_network = Column(Enum(SocialNetworks), nullable=False)
    cookies = Column(NestedMutationDict.as_mutable(JSONB), default=dict)

    campaign = relationship('Campaign',
                            secondary=CampaignAccount.__table__,
                            back_populates='accounts')

    def __str__(self):
        return f"<Account> {self.username} | {self.social_network}"


class Recipient(BaseMixin, Base):
    first_name = Column(String(), nullable=False)
    last_name = Column(String())
    profile_url = Column(String(), nullable=False)
    avatar_url = Column(String())
    social_network = Column(Enum(SocialNetworks), nullable=False)

    campaign = relationship('Campaign',
                            secondary=CampaignRecipient.__table__,
                            back_populates='recipients')

    def __str__(self):
        return f"<Recipient> {self.profile_url} | {self.social_network}"


class Message(BaseMixin, Base):
    text = Column(Text(), nullable=False)
    attachment = Column(NestedMutationDict.as_mutable(JSONB), default=dict)
    campaign_id = Column(Integer, ForeignKey('campaign.id'), nullable=False)
    campaign = relationship('Campaign', back_populates='messages')

    def __str__(self):
        return f"<Message> {self.campaign} | {self.text}"


class MessageDelivery(BaseMixin, Base):
    status = Column(Enum(MessageDeliveryStatuses), nullable=False)
    message_id = Column(Integer, ForeignKey('message.id'), nullable=False)
    recipient_id = Column(Integer, ForeignKey('recipient.id'), nullable=False)
    campaign_id = Column(Integer, ForeignKey('campaign.id'), nullable=False)
    campaign = relationship('Campaign',
                            back_populates='message_deliveries')

    def __str__(self):
        return f"<MessageDelivery> {self.campaign} | {self.message_id} | {self.status}"


class Campaign(BaseMixin, Base):
    name = Column(String(), nullable=False)
    description = Column(Text(), nullable=False)
    created_at = Column(UTCDateTime, default=now, nullable=False)
    updated_at = sa.Column(UTCDateTime, onupdate=now)
    owner_id = Column(ForeignKey('users.id'), nullable=False)
    owner = relationship('User')
    social_network = Column(Enum(SocialNetworks), nullable=False)
    status = Column(Enum(CampaignStatuses), nullable=False)

    workers = relationship('Worker',
                           secondary=CampaignWorker.__table__,
                           back_populates='campaign')
    recipients = relationship('Recipient',
                              secondary=CampaignRecipient.__table__,
                              back_populates='campaign')
    accounts = relationship('Account',
                            secondary=CampaignAccount.__table__,
                            back_populates='campaign')
    messages = relationship('Message',
                            back_populates='campaign')

    message_deliveries = relationship('MessageDelivery',
                                      back_populates='campaign')

    def __str__(self):
        return f"<Campaign> {self.name} | {self.social_network} | {self.status}"


class Worker(BaseMixin, Base):
    type = Column(Enum(WorkerType), nullable=False)
    availability = Column(Enum(WorkerAvailabilityStatuses), nullable=False)
    ip_address = Column(String(), default='127.0.0.1')
    user_id = Column(ForeignKey('users.id'), nullable=True)
    campaign = relationship('Campaign',
                            secondary=CampaignWorker.__table__,
                            back_populates='workers')

    browser = Column(Enum(AvailableBrowsers), nullable=True)

    def __str__(self):
        return f"<Message> {self.type} | {self.availability}"
