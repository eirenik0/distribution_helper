import random

import factory
from pytest_factoryboy import register

from distributionhelper.models import Account, User, Recipient, Worker
from distributionhelper.domain.constants import *

from sqlalchemy import orm
Session = orm.scoped_session(orm.sessionmaker())
#
#
# @register
# class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
#     username = factory.Faker('first_name')
#     hashed_password = factory.Faker('password')
#     email = factory.Faker('email')
#
#     class Meta:
#         model = User
#         sqlalchemy_session = Session


@register
class AccountFactory(factory.alchemy.SQLAlchemyModelFactory):
    username = factory.Faker('name')
    password = factory.Faker('password')
    profile_url = factory.Faker('url')
    cookies = {}
    social_network = random.choice(list(SocialNetworks)).name

    class Meta:
        model = Account
        sqlalchemy_session = Session

@register
class RecipientFactory(factory.alchemy.SQLAlchemyModelFactory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    profile_url = factory.Faker('url')
    avatar_url = factory.Faker('url')
    social_network = random.choice(list(SocialNetworks)).name

    class Meta:
        model = Recipient
        sqlalchemy_session = Session


# @register
# class CampaignFactory(factory.alchemy.SQLAlchemyModelFactory):
#     name = factory.Faker('name')
#
#     class Meta:
#         model = Campaign
#
#
# @register
# class MessageFactory(factory.alchemy.SQLAlchemyModelFactory):
#     text = factory.Faker('text')
#     attachement = {}
#
#     class Meta:
#         model = Message
#
# @register
# class WorkerFactory(factory.alchemy.SQLAlchemyModelFactory):
#     # avalability = random.choice(AVALABILITY_STATUSES)
#     ipv4_address = factory.Faker('ipv4')
#
#     class Meta:
#         model = Worker
