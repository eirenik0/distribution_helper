import uuid

import pytest

from distributionhelper.tests.factories.models import *


@pytest.fixture
def uid():
    return uuid.uuid1()
