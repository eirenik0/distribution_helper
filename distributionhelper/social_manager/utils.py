from functools import singledispatch, update_wrapper

from distributionhelper.social_manager.interfaces import IBrowserWorker


# def get_social_fabric(social, method):
#     """
#     Return adapter object regarding to social network
#     """
#     adapters = [YouTubeSocialAdapterAdapter, VKSeleniumAdapter, VKAPIAdapter]
#     bases = ()
#     for Adapter in adapters:
#         if (getattr(Adapter, '__social__') == social and
#             getattr(Adapter, '__method__') == method):
#             bases += (Adapter,)
#     return type(social, bases, {})


def check_captcha(f):
    """
    Helper decorator to class method. Check if captcha present on current page.
    Need to be implemented `is_captcha()` and `_captcha_proccess()` methods.
    """

    def wrapper(*args):
        self = args[0]
        if self.is_captcha():
            self._captcha_procces()
        return f(*args)

    return wrapper


def methdispatch(func):
    dispatcher = singledispatch(func)

    def wrapper(*args, **kw):
        return dispatcher.dispatch(args[1].__class__)(*args, **kw)

    wrapper.register = dispatcher.register
    update_wrapper(wrapper, func)
    return wrapper
